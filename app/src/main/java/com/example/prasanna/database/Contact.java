package com.example.prasanna.database;

/**
 * Created by Prasanna on 2/16/16.
 */
public class Contact {

    int ID;
    String Name,PhoneNumber;

    public Contact(int ID,String name,String PhoneNumber){
        this.ID = ID;
        this.Name = name;
        this.PhoneNumber = PhoneNumber;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }
}
